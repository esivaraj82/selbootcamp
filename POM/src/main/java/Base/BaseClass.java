package Base;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

import SeleniumBase.SeleniumBase;
import utils.DataLibrary;

public class BaseClass extends SeleniumBase {
	
	
	@DataProvider(name = "fetchData")
	public Object[][] fetchData() throws IOException {
		return DataLibrary.readExcelData(excelFileName);
	}	
  
  @BeforeMethod
  public void beforeMethod() {
	startApp("chrome", "https://login.salesforce.com");
	
  }

  @AfterMethod
  public void afterMethod() {
	  close();
  }

}
