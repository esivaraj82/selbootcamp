package Base;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import io.github.bonigarcia.wdm.WebDriverManager;
import utils.DataLibrary;

public class ProjectSpecificMethods extends DataLibrary{
	
	private static final ThreadLocal<RemoteWebDriver> remoteWebDriver = new ThreadLocal<RemoteWebDriver>();
	//public static ChromeDriver driver;
	public static  String excelFileName;
	public static ExtentHtmlReporter report;
	public static ExtentReports extent;
	public static ExtentTest test,node;
	public String testName, testDescription, testAuthor, testCategory;
	MediaEntityModelProvider img = null;

	@BeforeSuite
	public void startReport() {

		/*String date = new SimpleDateFormat("dd-MMM-YY").format(new Date());
		File folder= new File("./reports/"+date);
		if(!folder.exists()) {
			folder.mkdir();
		}*/
		report = new ExtentHtmlReporter("./reports/results.html");
		report.setAppendExisting(true);
		extent = new ExtentReports();
		extent.attachReporter(report);
	}

	@BeforeClass
	public void testDetails() {
		test = extent.createTest(testName, testDescription);
		test.assignAuthor(testAuthor);
		test.assignCategory(testCategory);

	}

	@BeforeMethod
	public void launchApplication() {

		node = test.createNode(testName);

		ChromeOptions options=new ChromeOptions();
		options.addArguments("--disable-notifications");
		WebDriverManager.chromedriver().setup();
		//remoteWebDriver.set(new ChromeDriver(options));
		setDriver();
		//driver = new ChromeDriver(options);

		//Launch the SalesForce URL
		getDriver().get("https://login.salesforce.com");
		getDriver().manage().window().maximize();
		getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(30));

	}

	@AfterMethod
	public void closeBrowser() {
		getDriver().close();
	}

	@AfterSuite
	public void stopReport() {
		extent.flush();
	}

	@DataProvider(name = "fetchData")
	public Object[][] fetchData() throws IOException {
		return utils.DataLibrary.readExcelData(excelFileName);
	}	

	public long takeSnap() throws IOException {
		long ranNum = (long) (Math.random() * 99999999999L);
		File source = getDriver().getScreenshotAs(OutputType.FILE);
		File target = new File("./snap/img "+ ranNum +".png");
		FileUtils.copyFile(source, target);
		return ranNum;
	}
	
	public void reportStep(String msg, String status) throws IOException{
		
		img = MediaEntityBuilder.createScreenCaptureFromPath(".././snap/img"+takeSnap()+".png").build();
		
		if(status.equalsIgnoreCase("Pass")) {
			node.pass(msg, img);
		} else if(status.equalsIgnoreCase("Fail")) {
			node.fail(msg,  img);
			throw new RuntimeException();
		}
	}
	
	public void setDriver() {
		remoteWebDriver.set(new ChromeDriver());
	}

	public RemoteWebDriver getDriver() {
		return remoteWebDriver.get();
	}
	
}

