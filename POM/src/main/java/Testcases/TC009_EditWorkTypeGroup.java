package Testcases;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Base.ProjectSpecificMethods;
import Pages.LoginPage;

public class TC009_EditWorkTypeGroup extends ProjectSpecificMethods {
	
	@BeforeTest
	public void setData() {
		testName = "TC009_Edit Work Type Group";
		testDescription = "Edit Work Type Group"; 
		testAuthor = "Sivaraj";
		testCategory = "Functional Testing";
		
	}
	
	@Test
	public void createWorkTypeGroup() throws InterruptedException, IOException {
		
		new LoginPage()
		.enterUserName()
		.enterPassword()
		.clickLogIn()
		.clickAppLauncher()
		.clickViewAll()
		.clickWorkTypeGroup()
		.searchWorkTypeGroupText()
		.selectEditDropDown()
		.editDescription()
		.editCapacity()
		.editWorkTypeSave()
		.verifyWorkTypeGroupText();
	}

}
