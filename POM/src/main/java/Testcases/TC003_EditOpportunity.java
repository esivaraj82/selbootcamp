package Testcases;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Base.ProjectSpecificMethods;
import Pages.LoginPage;

public class TC003_EditOpportunity extends ProjectSpecificMethods {
	
	@BeforeTest
	public void setData() {
		testName = "TC003_Edit Opportunity";
		testDescription = "Edit Opportunity"; 
		testAuthor = "Sivaraj";
		testCategory = "Functional Testing";
		
	}
	
	@Test
	public void editOpportunity() throws InterruptedException, IOException {
		
		new LoginPage()
		.enterUserName()
		.enterPassword()
		.clickLogIn()
		.clickAppLauncher()
		.clickViewAll()
		.clickSales()
		.clickOpportunity()
		.searchOpportunity()
		.clickEditButton()
		.editCloseDate()
		.editStage()
		.editDeliveryStatus()
		.editDescription()
		.editSave()
		.stageText();
	}

}
