package Testcases;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Base.ProjectSpecificMethods;
import Pages.LoginPage;

public class TC006_EditEntities extends ProjectSpecificMethods {
	
	@BeforeTest
	public void setData() {
		testName = "TC006_Edit Entities";
		testDescription = "Edit Entites"; 
		testAuthor = "Sivaraj";
		testCategory = "Functional Testing";
		
	}
	
	
	@Test
	public void editEntities() throws InterruptedException, IOException {
		
		new LoginPage()
		.enterUserName()
		.enterPassword()
		.clickLogIn()
		.clickAppLauncher()
		.clickViewAll()
		.clickLegalEntities()
		.searchLegalEntities()
		.clickEditLegalEntities()
		.editCompanyNameinLegalEntities()
		.editDescriptioninLegalEntities()
		.editStatusinLegalEntities()
		.editSaveinLegalEntities();		
	}

}
