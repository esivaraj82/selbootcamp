package Testcases;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Base.ProjectSpecificMethods;
import Pages.LoginPage;

public class TC012_EditIndividuals extends ProjectSpecificMethods {
	
	@BeforeTest
	public void setData() {
		testName = "TC012_Edit Individuals";
		testDescription = "Edit Individuals"; 
		testAuthor = "Sivaraj";
		testCategory = "Functional Testing";
		
	}
	
	@Test
	public void createIndividuals() throws InterruptedException, IOException {
		
		new LoginPage()
		.enterUserName()
		.enterPassword()
		.clickLogIn()
		.clickAppLauncher()
		.clickViewAll()
		.clickIndividuals()
		.clickIndividualstab()
		.searchName()
		.clickDropDown()
		.enterFirstName()
		.clickSaveEdit()
		.verifyFirstName();
	}

}
