package Testcases;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import Base.BaseClass;
import Pages.LoginPage;

public class TC002_CreateOpportunity extends BaseClass{

	@BeforeTest
	public void setData() {
		testcaseName = "TC002_Create Opportunity";
		testcaseDec = "Create Opportunity"; 
		author = "Sivaraj";
		category = "Functional Testing";
		excelFileName = "createOpportunity";
	}

	@Test(dataProvider="fetchData")
	public void createOpportunity(String name, String cdate) throws InterruptedException, IOException {
		new LoginPage()
		.enterUserName()
		.enterPassword()
		.clickLogIn()
		.clickAppLauncher()
		.clickViewAll()
		.clickSales()
		.clickOpportunity()
		.clickNewOpportunity()
		.enterOpportunityName(name)
		.enterCloseDate(cdate)
		.selectStage()
		.clickSave();
	}

}
