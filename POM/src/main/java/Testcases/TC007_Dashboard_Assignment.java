package Testcases;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Base.ProjectSpecificMethods;
import Pages.LoginPage;

public class TC007_Dashboard_Assignment extends ProjectSpecificMethods{

	@BeforeTest
	public void setData() {
		testName = "TC007_DashBoard Assignment";
		testDescription = "DashBoard Assignment"; 
		testAuthor = "Sivaraj";
		testCategory = "Functional Testing";
	}


	@Test
	public void dashboard() throws InterruptedException, IOException {

		new LoginPage()
		.enterUserName()
		.enterPassword()
		.clickLogIn()
		.clickAppLauncher()
		.clickViewAll()
		.clickServiceConsole()
		.clickDashBoardfromDropDown()
		.clickNewDashboard()
		.enterNameinDashboard()
		.enterDescriptioninDashboard()
		.verifyTitle()
		.clickDone()
		.verifyDashboardName()
		.clickSubscribe()
		.clickDaily()
		.chooseTime()
		.clickSubscribeSave()
		.verifyDashboardSubscription()
		.closeWorkOutTab()
		.clickPrivateDashBoards()
		.searchDashboardName()
		.clickDropDownitem()
		.clickDelete()
		.confirmDelete()
		.itemNotAvailable();
	}

}
