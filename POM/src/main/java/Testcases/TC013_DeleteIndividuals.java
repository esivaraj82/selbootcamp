package Testcases;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Base.ProjectSpecificMethods;
import Pages.LoginPage;

public class TC013_DeleteIndividuals extends ProjectSpecificMethods {
	
	@BeforeTest
	public void setData() {
		testName = "TC013_Delete Individuals";
		testDescription = "Delete Individuals"; 
		testAuthor = "Sivaraj";
		testCategory = "Functional Testing";
		
	}
	
	@Test
	public void createIndividuals() throws InterruptedException, IOException {
		
		new LoginPage()
		.enterUserName()
		.enterPassword()
		.clickLogIn()
		.clickAppLauncher()
		.clickViewAll()
		.clickIndividuals()
		.clickIndividualstab()
		.searchName()
		.clickDelete()
		.individualLastName()
		.confirmLastName();
	}

}
