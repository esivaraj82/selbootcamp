package Testcases;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Base.ProjectSpecificMethods;
import Pages.LoginPage;

public class TC005_CreateEntities extends ProjectSpecificMethods {
	
	@BeforeTest
	public void setData() {
		testName = "TC005_Create Entities";
		testDescription = "Create Entities"; 
		testAuthor = "Sivaraj";
		testCategory = "Functional Testing";
		
	}
	
	@Test
	public void createEntities() throws InterruptedException, IOException {
		
		new LoginPage()
		.enterUserName()
		.enterPassword()
		.clickLogIn()
		.clickAppLauncher()
		.clickViewAll()
		.clickLegalEntities()
		.clickNewLegalEntity()
		.enterNameLegalEntities()
		.clickSaveLegalEntities()
		.verifyLegalEntityName();
	}

}
