package Testcases;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Base.ProjectSpecificMethods;
import Pages.LoginPage;

public class TC008_CreateWorkTypeGroup extends ProjectSpecificMethods {
	
	@BeforeTest
	public void setData() {
		testName = "TC008_Create Work Type Group";
		testDescription = "Create Work Type Group"; 
		testAuthor = "Sivaraj";
		testCategory = "Functional Testing";
		
	}
	
	@Test
	public void createWorkTypeGroup() throws InterruptedException, IOException {
		
		new LoginPage()
		.enterUserName()
		.enterPassword()
		.clickLogIn()
		.clickAppLauncher()
		.clickViewAll()
		.clickWorkTypeGroup()
		.clickWorkTypeGroupsTab()
		.clickNewWorkTypeGroup()
		.enterWorkName()
		.clicksave()
		.verifyText();
	}

}
