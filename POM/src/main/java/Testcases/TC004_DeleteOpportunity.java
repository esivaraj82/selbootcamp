package Testcases;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Base.ProjectSpecificMethods;
import Pages.LoginPage;

public class TC004_DeleteOpportunity extends ProjectSpecificMethods {
	
	@BeforeTest
	public void setData() {
		testName = "TC004_Delete Opportunity";
		testDescription = "Delete Opportunity"; 
		testAuthor = "Sivaraj";
		testCategory = "Functional Testing";
		
	}

	@Test
	public void deleteOpportunity() throws InterruptedException, IOException {

		new LoginPage()
		.enterUserName()
		.enterPassword()
		.clickLogIn()
		.clickAppLauncher()
		.clickViewAll()
		.clickSales()
		.clickOpportunity()
		.searchOpportunity()
		.clickDelete()
		.verifyText();

	}

}
