package Testcases;

import java.io.IOException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import Base.BaseClass;
import Pages.LoginPage;

public class TC001_LogIn extends BaseClass{

	@BeforeTest
	public void setData() {
		testcaseName = "TC001_Login Page";
		testcaseDec = "Login"; 
		author = "Sivaraj";
		category = "Functional Testing";
	}

	@Test
	public void runLogIn() throws InterruptedException, IOException {

		new LoginPage()
		.enterUserName()
		.enterPassword()
		.clickLogIn();
	}

}
