package Pages;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import Base.BaseClass;


public class MyOpportunity extends BaseClass {
	
	
	public MyOpportunity() {
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how=How.XPATH, using="//div[@title='New']")  WebElement clickNewOpportunity;
	@FindBy(how=How.XPATH, using="//label[text()='Opportunity Name']/following::input")  WebElement enterOpportunityName;
	@FindBy(how=How.XPATH, using="(//input[@class='slds-input'])[3]")  WebElement enterCloseDate;
	@FindBy(how=How.XPATH, using="(//input[@class='slds-input slds-combobox__input'])[3]")  WebElement selectStage;
	@FindBy(how=How.XPATH, using="//span[@title='Needs Analysis']")  WebElement chooseStage;
	@FindBy(how=How.XPATH, using="//button[@name='SaveEdit']")  WebElement clickSave;
	@FindBy(how=How.XPATH, using="//input[@placeholder='Search this list...']")  WebElement searchOpportunity;
	@FindBy(how=How.XPATH, using="//span[@class='slds-icon_container slds-icon-utility-down']//span")  WebElement clickEditButton;
	@FindBy(how=How.XPATH, using="//a[@title='Edit']")  WebElement clickEdit;
	@FindBy(how=How.XPATH, using="//input[@name='CloseDate']")  WebElement editCloseDate;
	@FindBy(how=How.XPATH, using="(//input[@class='slds-input slds-combobox__input'])[4]")  WebElement editStage;
	@FindBy(how=How.XPATH, using="//span[@title='Perception Analysis']")  WebElement PerceptionAnalysis;
	@FindBy(how=How.XPATH, using="//label[text()='Delivery/Installation Status']/following::input")  WebElement editDeliveryStatus;
	@FindBy(how=How.XPATH, using="//span[@title='In progress']")  WebElement Inprogress;
	@FindBy(how=How.XPATH, using="//textarea[@class='slds-textarea']")  WebElement editDescription;
	@FindBy(how=How.XPATH, using="//span[@class='slds-icon_container slds-icon-utility-down']//span")  WebElement clickDelete;
	@FindBy(how=How.XPATH, using="//a[@title='Delete']")  WebElement Delete;
	@FindBy(how=How.XPATH, using="//span[text()='Delete']")  WebElement confirmDelete;
	@FindBy(how=How.XPATH, using="//button[@name='SaveEdit']")  WebElement editSave;
	@FindBy(how=How.XPATH, using="//span[text()='Perception Analysis']")  WebElement stageText;
	@FindBy(how=How.XPATH, using="//input[@placeholder='Search this list...']")  WebElement verifyText;
	@FindBy(how=How.XPATH, using="//span[text()='No items to display.']")  WebElement verifyTextConfirm;

	
	
	public MyOpportunity clickNewOpportunity() throws IOException {
		click(clickNewOpportunity);
		takeSnap();
		return this;

	}

	public MyOpportunity enterOpportunityName(String uName) throws IOException {
		click(enterOpportunityName);
		takeSnap();
		return this;
	}

	public MyOpportunity enterCloseDate(String cDate) throws IOException {
		clearAndType(enterCloseDate, cDate);
		takeSnap();
		return this;
	}

	public MyOpportunity selectStage() throws InterruptedException, IOException {
		click(selectStage);
		takeSnap();
		click(chooseStage);
		takeSnap();
		return this;
	}
	public MyOpportunity clickSave() throws IOException {
		click(clickSave);
		takeSnap();
		return this;
	}

	//Search the Opportunity
	public MyOpportunity searchOpportunity() throws InterruptedException, IOException {
		
		clearAndTypeAndEnter(searchOpportunity, "Salesforce Automation by Sivaraj");
		takeSnap();	
		return this;
	}

	//Click the edit button
	public MyOpportunity clickEditButton() throws InterruptedException, IOException {
			
		click(clickEditButton);
		click(clickEdit);
		takeSnap();
		return this;
	}

	//Choose close date as Tomorrow date
	public MyOpportunity editCloseDate() throws IOException {
		clearAndType(editCloseDate, "11/14/2021" );
		takeSnap();
		return this;
	}

	//Select 'Stage' as Perception Analysis
	public MyOpportunity editStage() throws InterruptedException, IOException {
		
		click(editStage);
		click(PerceptionAnalysis);
		takeSnap();
		return this;
	}

	//Select Deliver Status as In Progress
	public MyOpportunity editDeliveryStatus() throws InterruptedException, IOException {
		
		click(editDeliveryStatus);
		click(Inprogress);
		takeSnap();
		return this;
	}

	//Enter Description as SalesForce
	public MyOpportunity editDescription() throws IOException {
		
		clearAndType(editDescription, "SalesForce");
		takeSnap();
		return this ;
	}

	//Click on Save
	public MyOpportunity editSave() throws InterruptedException, IOException {
		click(editSave);
		takeSnap();
		return this;
	}

	//Verify Stage as Perception Analysis
	public MyOpportunity stageText() throws IOException {
		
		String perception = getElementText(stageText);
		System.out.println(perception);
		takeSnap();
		return this;
	}

	//click Delete button
	public MyOpportunity clickDelete() throws InterruptedException, IOException {
		
		click(clickDelete);
		takeSnap();
		click(Delete);
		takeSnap();
		click(confirmDelete);
		takeSnap();
		return this;
	}

	//Vereify the text matches
	public MyOpportunity verifyText() throws InterruptedException, IOException {
		
		clearAndTypeAndEnter(verifyText, "Salesforce Automation by Sivaraj");
		String nomatches = getElementText(verifyTextConfirm);
		System.out.println(nomatches);
		takeSnap();
		return this;

	}
}