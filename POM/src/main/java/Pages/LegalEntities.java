package Pages;

import java.io.IOException;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import Base.BaseClass;


public class LegalEntities extends BaseClass {
	
	public LegalEntities() {
		PageFactory.initElements(driver, this); 
	}

	@FindBy(how=How.XPATH, using="//div[text()='New']")  WebElement clickNewLegalEntity;
	@FindBy(how=How.XPATH, using="//input[@class=' input']")  WebElement enterNameLegalEntities;
	@FindBy(how=How.XPATH, using="(//span[text()='Save'])[2]")  WebElement clickSaveLegalEntities;
	@FindBy(how=How.XPATH, using="(//span[@class='uiOutputText'])[3]")  WebElement verifyLegalEntityName;
	@FindBy(how=How.XPATH, using="//input[@placeholder='Search this list...']")  WebElement searchLegalEntities;
	@FindBy(how=How.XPATH, using="//span[@class='slds-icon_container slds-icon-utility-down']//span")  WebElement clickEditLegalEntities;
	@FindBy(how=How.XPATH, using="//a[@title='Edit']")  WebElement clickEditLegalEntitiesEdit;
	@FindBy(how=How.XPATH, using="(//input[@class=' input'])[2]")  WebElement editCompanyNameinLegalEntities;
	@FindBy(how=How.XPATH, using="//textarea[@class=' textarea']")  WebElement editDescriptioninLegalEntities;
	@FindBy(how=How.XPATH, using="//a[@class='select']")  WebElement editStatusinLegalEntitiesSelect;
	@FindBy(how=How.XPATH, using="//a[text()='Active']")  WebElement editStatusinLegalEntitiesActive;
	@FindBy(how=How.XPATH, using="(//span[text()='Save'])[2]")  WebElement editSaveinLegalEntities;
	@FindBy(how=How.XPATH, using="//a[contains(@class,'slds-truncate outputLookupLink')]")  WebElement editSaveinLegalEntities2;
	@FindBy(how=How.XPATH, using="//span[text()='Active']")  WebElement editSaveinLegalEntitiesText;
	
	//Click on New Legal Entity
	public LegalEntities clickNewLegalEntity() throws InterruptedException, IOException {
		click(clickNewLegalEntity);
		takeSnap();
		return this;

	}
	//Enter Name as 'Salesforce Automation by Your Name'
	public LegalEntities enterNameLegalEntities() throws IOException {
		
		clearAndType(enterNameLegalEntities, "Salesforce Automation by Sivaraj");
		takeSnap();
		return this;
	}

	//click Save
	public LegalEntities clickSaveLegalEntities() throws InterruptedException, IOException {
		click(clickSaveLegalEntities);
		takeSnap();
		return this;
	}

	//verify Legal Entity Name
	public LegalEntities verifyLegalEntityName() throws IOException {
		
		String verifyText = getElementText(verifyLegalEntityName);
		System.out.println(verifyText);
		takeSnap();
		return this;
	}

	//Search the Legal Entity 'Salesforce Automation by Your Name'
	public LegalEntities searchLegalEntities() throws InterruptedException, IOException {
		
		clearAndTypeAndEnter(searchLegalEntities, "Salesforce Automation by Sivaraj");
		takeSnap();
		return this;
	}

	//Click on the Dropdown icon and Select Edit
	public LegalEntities clickEditLegalEntities() throws InterruptedException, IOException {
			clickwithExecutor(clickEditLegalEntities);
			click(clickEditLegalEntitiesEdit);
			takeSnap();
		return this;
	}

	//Enter the Company name as 'Testleaf'
	public LegalEntities editCompanyNameinLegalEntities() throws IOException {
		
		clearAndType(editCompanyNameinLegalEntities, "TestLeaf");
		takeSnap();
		return this;
	}

	//Enter Description as 'SalesForce'.
	public LegalEntities editDescriptioninLegalEntities() throws InterruptedException, IOException {
		
		clearAndType(editDescriptioninLegalEntities, "SalesForce");
		takeSnap();
		return this;
	}

	//Select Status as 'Active'
	public LegalEntities editStatusinLegalEntities() throws IOException {
		
		click(editStatusinLegalEntitiesSelect);
		click(editStatusinLegalEntitiesActive);
		takeSnap();
		return this;
	}

	//Click on Save and Verify Status as Active
	public LegalEntities editSaveinLegalEntities() throws InterruptedException, IOException {
		
		click(editSaveinLegalEntities);
		click(editSaveinLegalEntities2);
		String status = getElementText(editSaveinLegalEntitiesText);
		System.out.println(status);
		takeSnap();
		return this;
	}
}
