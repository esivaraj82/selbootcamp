package Pages;

import java.io.IOException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import Base.BaseClass;


public class HomePage extends BaseClass {
	
	public HomePage() {
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how=How.CLASS_NAME, using="slds-r5")  WebElement clickAppLauncher;
	@FindBy(how=How.XPATH, using="//button[text()='View All']")  WebElement clickViewAll;
	@FindBy(how=How.XPATH, using="//p[text()='Sales']")  WebElement clickSales;
	@FindBy(how=How.XPATH, using="//p[text()='Legal Entities']")  WebElement clickLegalEntities;
	@FindBy(how=How.XPATH, using="//p[text()='Service Console']")  WebElement serviceConsole;
	@FindBy(how=How.XPATH, using="//p[text()='Work Type Groups']")  WebElement workTypeGroup;
	@FindBy(how=How.XPATH, using="//p[text()='Individuals']")  WebElement clickIndividuals;
	
	public HomePage clickAppLauncher() throws IOException {
		click(clickAppLauncher);
		takeSnap();		
		return this;
	}


	//Click view All
	public HomePage clickViewAll() throws IOException {

		click(clickViewAll);
		takeSnap();
		return this;
	}

	//click Sales from App Launcher
	public MyHomePage clickSales() throws IOException  {
		
		click(clickSales);
		takeSnap();
		return new MyHomePage();

	}

	//click Legal Entity from App Launcher
	public LegalEntities clickLegalEntities() throws InterruptedException, IOException {
		click(clickLegalEntities);
		takeSnap();
		return new LegalEntities();

	}
	
	//click Service Console from App Launcher
		public DashBoard clickServiceConsole() throws InterruptedException, IOException {
			click(serviceConsole);
			takeSnap();
			return new DashBoard();

		}
		
	//Click Work Type Group from App Launcher
		public WorkTypeGroup clickWorkTypeGroup() throws InterruptedException, IOException {
			click(workTypeGroup);
			takeSnap();
			return new WorkTypeGroup();
		}
		
		//click Individuals from App Launcher
		public Individuals clickIndividuals() {
			clickwithExecutor(clickIndividuals);
			takeSnap();
			return new Individuals();
			
		}
		
}
