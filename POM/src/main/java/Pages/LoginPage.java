package Pages;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import Base.BaseClass;

public class LoginPage extends BaseClass{
	
	public LoginPage() {
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how=How.ID, using="username")  WebElement eleUserName;
	@FindBy(how=How.ID, using="password")  WebElement elePassword;
	@FindBy(how=How.ID, using="Login")  WebElement clickLogin;
	
	
	public LoginPage enterUserName() throws IOException {
			
		clearAndType(eleUserName, "makaia@testleaf.com");
		takeSnap();
		return this;
	}

	
	public LoginPage enterPassword() throws IOException {
		
		clearAndType(elePassword, "SelBootcamp@1234");
		takeSnap();
		return this;
	}


	public HomePage clickLogIn() throws InterruptedException, IOException {
		click(clickLogin);
		takeSnap();
		return new HomePage();
	}


}
