package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import Base.BaseClass;

public class Individuals extends BaseClass {


	public Individuals() {
		PageFactory.initElements(driver, this); 
	}

	@FindBy(how=How.XPATH, using="//span[contains(text(),'Individuals Menu')]")  WebElement clickIndividualstab;
	@FindBy(how=How.XPATH, using="//span[text()='New Individual']")  WebElement clickNewIndividuals;
	@FindBy(how=How.XPATH, using="//a[text()='--None--']")  WebElement enterNone;
	@FindBy(how=How.XPATH, using="//a[@title='Mr.']")  WebElement enterClick;
	@FindBy(how=How.XPATH, using="//input[@placeholder='Last Name']")  WebElement enterLastName;
	@FindBy(how=How.XPATH, using="(//span[text()='Save'])[2]")  WebElement clickSave;
	@FindBy(how=How.XPATH, using="//input[@placeholder='Search this list...']")  WebElement searchName;
	@FindBy(how=How.XPATH, using="//span[@class='slds-icon_container slds-icon-utility-down']//span")  WebElement clickDropDown;
	@FindBy(how=How.XPATH, using="//a[@title='Edit']")  WebElement clickEdit;
	@FindBy(how=How.XPATH, using="//input[@class='firstName compoundBorderBottom form-element__row input']")  WebElement enterFirstName;
	@FindBy(how=How.XPATH, using="(//span[text()='Save'])[2]")  WebElement clickSaveEdit;
	@FindBy(how=How.XPATH, using="(//a[contains(@class,'slds-truncate outputLookupLink')])[1]")  WebElement verifyFirstName;
	@FindBy(how=How.XPATH, using="//span[@class='slds-icon_container slds-icon-utility-down']//span")  WebElement clickDelete;
	@FindBy(how=How.XPATH, using="//div[text()='Delete']")  WebElement Delete;
	@FindBy(how=How.XPATH, using="//input[@placeholder='Search this list...']")  WebElement individualLastName;
	@FindBy(how=How.XPATH, using="//span[text()='No items to display.']")  WebElement confirmLastName;


	//Click on the Dropdown icon in the Individuals tab
	public Individuals clickIndividualstab() {
		clickwithExecutor(clickIndividualstab);
		takeSnap();
		return this;
	}


	//Click on New Individual
	public Individuals clickNewIndividuals() {
		clickwithExecutor(clickNewIndividuals);
		takeSnap();
		return this;
	}

	//Enter the Last Name as 'Kumar'
	public Individuals enterLastName() {

		clickwithExecutor(enterNone);	
		click(enterClick);
		clearAndType(enterLastName, "Kumar");
		takeSnap();
		return this;
	}

	//click Save
	public Individuals clickSave() {
		click(clickSave);
		takeSnap();
		return this;
	}


	//Search the Individuals 'Kumar' & Click on the Dropdown icon and Select Edit
	public Individuals searchName() {	
		clearAndTypeAndEnter(searchName, "Kumar");
		takeSnap();
		return this;
	}

	//Click on the Dropdown icon and Select Edit
	public Individuals clickDropDown() {

		click(clickDropDown);
		click(clickEdit);
		takeSnap();
		return this;
	}

	//Enter the first name as 'Ganesh'.
	public Individuals enterFirstName() {
		clearAndType(enterFirstName, "Ganesh");
		takeSnap();
		return this;
	}


	//Click on Save
	public Individuals clickSaveEdit() {
		click(clickSaveEdit);
		takeSnap();
		return this;
	}


	//Verify the first name as 'Ganesh'
	public Individuals verifyFirstName() {

		String fName = getElementText(verifyFirstName);
		System.out.println(fName);
		return this;
	}

	//Click on the Dropdown icon and Select Delete
	public Individuals clickDelete() {
		click(clickDelete);
		clickwithExecutor(Delete);
		takeSnap();
		return this;
	}

	//Verify Whether Individual is Deleted using Individual last name
	public Individuals individualLastName() {
		clearAndTypeAndEnter(individualLastName, "Kumar");
		takeSnap();
		return this;
	}


	public Individuals confirmLastName() {
		
		String lName = getElementText(confirmLastName);
		System.out.println(lName);
		return this;
	}

}