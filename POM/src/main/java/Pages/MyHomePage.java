package Pages;

import java.io.IOException;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import Base.BaseClass;


public class MyHomePage extends BaseClass{

	public MyHomePage() {
		PageFactory.initElements(driver, this); 
	}

	@FindBy(how=How.XPATH, using="//span[text()='Opportunities']")  WebElement clickOpportunity;

	public MyOpportunity clickOpportunity() throws IOException {

		click(clickOpportunity);
		takeSnap();
		return new MyOpportunity();
	}



}
