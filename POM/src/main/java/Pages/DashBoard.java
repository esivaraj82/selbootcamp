package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import Base.BaseClass;
import Base.ProjectSpecificMethods;

public class DashBoard extends BaseClass{
	
	public DashBoard() {
		PageFactory.initElements(driver, this); 
	}

	
	@FindBy(how=How.XPATH, using="//span[text()='Show Navigation Menu']")  WebElement clickHomefromDropDown;
	@FindBy(how=How.XPATH, using="//span[text()='Dashboards']")  WebElement clickDashBoardfromDropDown;
	@FindBy(how=How.XPATH, using="//div[text()='New Dashboard']")  WebElement clickNewDashboard;
	@FindBy(how=How.XPATH, using="//input[@id='dashboardNameInput']")  WebElement enterNameinDashboard;
	@FindBy(how=How.XPATH, using="//input[@id='dashboardDescriptionInput']")  WebElement enterDescriptioninDashboard;
	@FindBy(how=How.ID, using="submitBtn")  WebElement enterDescriptioninDashboardSubmit;
	@FindBy(how=How.XPATH, using="//div[@class='toolbarActions']/button")  WebElement clickDone;
	@FindBy(how=How.XPATH, using="//span[@class='slds-page-header__title slds-truncate']")  WebElement verifyDashboardName;
	@FindBy(how=How.XPATH, using="//button[text()='Subscribe']")  WebElement clickSubscribe;
	@FindBy(how=How.XPATH, using="//span[text()='Daily']")  WebElement clickDaily;
	@FindBy(how=How.XPATH, using="//span[text()='Save']")  WebElement clickSubscribeSave;
	
	//Select Home from the DropDown
	public DashBoard clickHomefromDropDown() throws InterruptedException {
		clickwithExecutor(clickHomefromDropDown);
		return this;
	}

	//Select Dashboards from DropDown
	public DashBoard clickDashBoardfromDropDown() throws InterruptedException {
		driver.executeScript("arguments[0].click()", driver.findElement(By.xpath("//span[text()='Show Navigation Menu']")));
		clickwithExecutor(clickDashBoardfromDropDown);
		return this;
	}

	//Click on New Dashboard
	public DashBoard clickNewDashboard() throws InterruptedException {
		click(clickNewDashboard);
		return this;
	}

	//Enter the Dashboard name as "YourName_Workout"
	public DashBoard enterNameinDashboard() throws InterruptedException {
		WebElement frame = driver.findElement(By.xpath("//iframe[@title='dashboard']"));
		driver.switchTo().frame(frame);
		clearAndType(enterNameinDashboard, "Sivaraj");
		return this;
	}
	//Enter Description as Testing and Click on Create
	public DashBoard enterDescriptioninDashboard() throws InterruptedException {
		clearAndType(enterDescriptioninDashboard, "Testing");
		click(enterDescriptioninDashboardSubmit);
		driver.switchTo().defaultContent();
		return this;
	}

	//Verify Title
	public DashBoard verifyTitle() throws InterruptedException {
		String browserTitle = driver.getTitle();
		System.out.println("Browser Title: "+browserTitle);
		Thread.sleep(5000);
		return this;
	}

	//Click on Done
	public DashBoard clickDone() throws InterruptedException {
		WebElement frame = driver.findElement(By.xpath("//iframe[@title='dashboard']"));
		driver.switchTo().frame(frame);
		clickwithExecutor(clickDone);
		return this;
	}

	//Verify the Dashboard is Created
	public DashBoard verifyDashboardName()  {
		String verifyDashboard = getElementText(verifyDashboardName);
		System.out.println("Dashboard Name :"+verifyDashboard);
		return this;
	}
	
	//Click on Subscribe
	public DashBoard clickSubscribe() throws InterruptedException  {
		click(clickSubscribe);
		return this;
	}

	//Select Frequency as "Daily"
	public DashBoard clickDaily() throws InterruptedException  {
		driver.switchTo().defaultContent();
		clickwithExecutor(clickDaily);
		return this;
	}

	//Time as 10:00 AM
	public DashBoard chooseTime() throws InterruptedException  {
		WebElement time = driver.findElement(By.xpath("//select[@class=' select']"));
		Select sel = new Select(time);
		sel.selectByVisibleText("10:00 AM");
		Thread.sleep(5000);
		return this;
	}

	//Click on Save
	public DashBoard clickSubscribeSave() throws InterruptedException  {
		click(clickSubscribeSave);
		return this;
	}

	//Verify "You started Dashboard Subscription" message displayed or not
	public DashBoard verifyDashboardSubscription() {
		String verifyName = driver.findElement(By.xpath("//span[text()='You started a dashboard subscription.']")).getText();
		System.out.println("DashBoard Subscription Veriifcation:" +verifyName);
		return this;
	}

	//Close the "YourName_Workout" tab
	public DashBoard closeWorkOutTab() throws InterruptedException {
		driver.executeScript("arguments[0].click()",driver.findElement(By.xpath("//span[text()='Actions for Sivaraj']")));
		Thread.sleep(4000);
		driver.executeScript("arguments[0].click()",driver.findElement(By.xpath("//span[text()='Close Tab']")));
		Thread.sleep(5000);
		return this;
	}

	//Click on Private Dashboards
	public DashBoard clickPrivateDashBoards() throws InterruptedException {
		driver.findElement(By.xpath("//a[text()='Private Dashboards']")).click();
		Thread.sleep(5000);
		return this;
	}

	//Verify the newly created Dashboard available
	public DashBoard searchDashboardName() throws InterruptedException {
		driver.findElement(By.xpath("//input[@placeholder='Search private dashboards...']")).sendKeys("Sivaraj", Keys.ENTER);
		Thread.sleep(5000);
		return this;
	}

	//Click on dropdown for the item
	public DashBoard clickDropDownitem() throws InterruptedException {
		driver.executeScript("arguments[0].click()", driver.findElement(By.xpath("//button[@class='slds-button slds-button_icon-border slds-button_icon-x-small']")));
		Thread.sleep(5000);
		return this;
	}

	//Select Delete
	public DashBoard clickDelete() throws InterruptedException {
		driver.executeScript("arguments[0].click()", driver.findElement(By.xpath("//span[text()='Delete']")));
		Thread.sleep(5000);
		driver.executeScript("arguments[0].click()",driver.findElement(By.xpath("//span[text()='Close this window']")));
		return this;
	}

	//Confirm the Delete
	public DashBoard confirmDelete() throws InterruptedException {
		driver.executeScript("arguments[0].click()", driver.findElement(By.xpath("//span[text()='Delete']")));
		Thread.sleep(5000);
		driver.executeScript("arguments[0].click()", driver.findElement(By.xpath("//button[@title='Delete']/span")));
		Thread.sleep(5000);
		//String deleteText = driver.findElement(By.xpath("//span[contains(text(), 'Dashboard']")).getText();
		//System.out.println("Delete Confirmation:" +deleteText);
		return this;
	}


	//Verify the item is not available under Private Dashboard folder
	public DashBoard itemNotAvailable() throws InterruptedException {
		String item = driver.findElement(By.xpath("//span[@class='emptyMessageTitle']")).getText();
		System.out.println(item);
		return this;
	}

}
