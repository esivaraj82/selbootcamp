package Pages;

import java.io.IOException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import Base.BaseClass;


public class WorkTypeGroup extends BaseClass{

	public WorkTypeGroup() {
		PageFactory.initElements(driver, this); 
	}

	@FindBy(how=How.XPATH, using="(//span[contains(text(),'Work Type Groups')])[3]")  WebElement clickWorkTypeGroupsTab;
	@FindBy(how=How.XPATH, using="//span[text()='New Work Type Group']")  WebElement clickNewWorkTypeGroup;
	@FindBy(how=How.XPATH, using="//input[@class=' input']")  WebElement enterWorkName;
	@FindBy(how=How.XPATH, using="(//span[text()='Save'])[2]")  WebElement clicksave;
	@FindBy(how=How.XPATH, using="(//span[@class='uiOutputText'])[2]")  WebElement verifyText;
	@FindBy(how=How.XPATH, using="//input[@placeholder='Search this list...']")  WebElement searchWorkTypeGroupText;
	@FindBy(how=How.XPATH, using="//span[@class='slds-icon_container slds-icon-utility-down']//span")  WebElement selectEditDropDown;
	@FindBy(how=How.XPATH, using="//a[@title='Edit']")  WebElement selectEditDropDown2;
	@FindBy(how=How.XPATH, using="//textarea[@class=' textarea']")  WebElement editDescription;
	@FindBy(how=How.CLASS_NAME, using="select")  WebElement editCapacity;
	@FindBy(how=How.XPATH, using="//a[@title='Capacity']")  WebElement capacity;
	@FindBy(how=How.XPATH, using="(//span[text()='Save'])[2]")  WebElement editWorkTypeSave;
	@FindBy(how=How.XPATH, using="(//a[@data-aura-class='forceOutputLookup'])[1]")  WebElement verifyWorkTypeGroupText;
	@FindBy(how=How.XPATH, using="//span[@data-aura-class='uiOutputTextArea']")  WebElement verifyWorkTypeGroup;

	//Click on the Dropdown icon in the Work Type Groups tab
	public WorkTypeGroup clickWorkTypeGroupsTab() throws InterruptedException, IOException {

		clickwithExecutor(clickWorkTypeGroupsTab);
		takeSnap();
		return this;
	}

	//Click on New Work Type Group
	public WorkTypeGroup clickNewWorkTypeGroup() throws InterruptedException, IOException {
		clickwithExecutor(clickNewWorkTypeGroup);
		takeSnap();
		return this;
	}

	//Enter Work Type Group Name as 'Salesforce Automation by Your Name'
	public WorkTypeGroup enterWorkName() throws InterruptedException, IOException {

		clearAndType(enterWorkName, "Salesforce Automation by Sivaraj");
		takeSnap();
		return this;
	}

	//click Save
	public WorkTypeGroup clicksave() throws InterruptedException, IOException {

		click(clicksave);
		takeSnap();
		return this;
	}

	//verify Work Type Group Name
	public WorkTypeGroup verifyText() throws IOException {

		String verify = getElementText(verifyText);
		System.out.println(verify);
		takeSnap();
		return this;
	}

	//Search the Work Type Group 'Salesforce Automation by Your Name'
	public WorkTypeGroup searchWorkTypeGroupText() throws InterruptedException, IOException {
		clearAndTypeAndEnter(searchWorkTypeGroupText, "Salesforce Automation by Sivaraj");
		takeSnap();
		return this;
	}

	//Click on the Dropdown icon and Select Edit
	public WorkTypeGroup selectEditDropDown() throws InterruptedException, IOException {

		click(selectEditDropDown);
		click(selectEditDropDown2);
		takeSnap();
		return this;
	}

	//Enter Description as 'Automation'.
	public WorkTypeGroup editDescription() throws InterruptedException, IOException {

		clearAndType(editDescription, "Automation");
		takeSnap();
		return this;
	}


	//Select Group Type as 'Capacity'
	public WorkTypeGroup editCapacity() throws InterruptedException, IOException {

		click(editCapacity);
		click(capacity);
		takeSnap();
		return this;
	}

	//Click on Save
	public WorkTypeGroup editWorkTypeSave() throws InterruptedException, IOException {

		click(editWorkTypeSave);
		takeSnap();
		return this;
	}

	//Click on 'Salesforce Automation by Your Name'and Verify Description as 'Automation'
	public WorkTypeGroup verifyWorkTypeGroupText() throws InterruptedException, IOException {

		click(verifyWorkTypeGroupText);
		String verifyDescription = getElementText(verifyWorkTypeGroup);
		System.out.println(verifyDescription);
		takeSnap();
		return this;
	}
}
